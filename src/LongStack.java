import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LongStack {

   private LinkedList<Long> stack;

   public static void main (String[] argum) {
       System.out.println("Hello");
   }

   LongStack() {
       stack = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack tmp = new LongStack();

      for (int i = 0; i < stack.size(); i++) {
         tmp.stack.add(stack.get(i));
      }

      return tmp;
   }

   public boolean stEmpty() {
      if  (stack.size() == 0) {
         return true;
      }
      return false;
   }

   public void push (long a) {
      stack.addLast(a);
   }

   public long pop() {
      if (stEmpty()) {
         throw new RuntimeException("Stack is empty");
      }
      return (long) stack.removeLast();
   } // pop

   public void op (String s) {
      if (stack.size() >= 2) {
         long op2 = pop();
         long op1 = pop();
         if (s.equals("+")) { push(op1 + op2); }
         if (s.equals("-")) { push(op1 - op2); }
         if (s.equals("*")) { push(op1 * op2); }
         if (s.equals("/")) { push(op1 / op2); }
      } else {
          throw new RuntimeException("Stack size not sufficient for " + s);
      }
   }
  
   public long tos() {
      if(!stEmpty()) {
          return stack.getLast();
      } else {
          throw new RuntimeException("Stack is empty! Can't return last!");
      }

   }

   @Override
   public boolean equals (Object o) {
      if (stack.size() != ((LongStack) o).stack.size())
          return false;
      if (stEmpty() && ((LongStack) o).stEmpty()) {
          return true;
      }
      for (int i = 0; i < stack.size(); i++) {
           if (stack.get(i) != (long) ((LongStack) o).stack.get(i)) {
               return false;
           }
      }
      return true;
   }

   @Override
   public String toString() {
      if (stack.size() > 0) {
          StringBuffer sb = new StringBuffer();

          for (int i = 0; i < stack.size(); i++) {
              sb.append(stack.get(i).toString() + " ");
          }
          return sb.toString();
      }
      return "";
   }

   public static long interpret (String pol) {
       List<String> operators = Arrays.asList("+", "-", "/", "*");
       String[] tokens;
       try {
           tokens = pol.split(" ");
       } catch (Exception e) {
           throw new RuntimeException("Unable to parse tokens from string" + pol);
       }

       LongStack ls = new LongStack();

       for (String t : tokens) {
           if (operators.contains(t)) {
               if (ls.stack.size() < 2) {
                   throw new RuntimeException("Stack size not sufficient. You need at least two numbers to process " + t + " for string " + pol);
               }
               ls.op(t);
           } else if (t.contains("\t")) {
               if (!t.equals("\t")) {
                   ls.push(Long.parseLong(t.replace("\t", "")));
               }
           } else if (!t.equals("")) {
               ls.push(Long.parseLong(t));
           }

       }

       if (ls.stack.size() > 1) {
           throw new RuntimeException("User input has too many values and not enough operators to process the values for string " + pol);
       }
       return ls.tos();
   }

}

